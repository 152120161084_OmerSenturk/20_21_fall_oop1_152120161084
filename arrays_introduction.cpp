﻿#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

void print(int arr[], int count) {
	for (int i = 0; i < count; ++i)
		cout << arr[i] << ' ';

	cout << '\n';
}

void reverse(int arr[], int count) {
	int temp;
	for (int i = 0; i < count / 2; ++i) {
		temp = arr[i];
		arr[i] = arr[count - i - 1];
		arr[count - i - 1] = temp;
	}
}
int main() {
	/* Enter your code here. Read input from STDIN. Print output to STDOUT */
	int a;
	cin >> a;

	int arr[a];

	for (int i = 0; i < a; ++i) {
		int tmp;
		cin >> tmp;
		arr[i] = tmp;
	}

	reverse(arr, a);

	print(arr, a);

	return 0;
}
